<?php
/**
 * Template Name: PHP Posts - WP REST API
 */
get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">


<?php 
    $response = wp_remote_get( 'http://iqq.pro100photo.in.ua/wp-json/wp/v2/posts' );
     
     $posts = json_decode( wp_remote_retrieve_body( $response ) ); //decode json in php
     $format = "d M Y"; //convert date iso 8601 date in day-month-year
    
   
     if( empty( $posts ) ) {
     return;
     }
     
     // Main Content Here
     if( !empty( $posts ) ) {
      

     foreach( $posts as $post ) {
     echo '<article id="post-' . $post->id . '" class="post-' . $post->id . ' post type-post status-publish format-standard hentry">';
     echo '<header class="entry-header">';
     echo '<h2 class="entry-title">';
     echo '<a href="' . $post->link . '">' . $post->title->rendered . '<sup class="apipost"> POST from API</sup>' . '</a>' . '</h2>' . '</header>';
     echo '<div class="entry-content">' . $post->content->rendered . '</div>';
     echo '<footer class="entry-footer">' . '<span class="posted-on">' . '<a href="' . $post->link . '" rel="bookmark">' . '<time class="entry-date published updated" datetime="' . date_format(date_create($post->date), $format) . '">' . date_format(date_create($post->date), $format) . '</time>' . '</a>' . '</span>';
     echo '<span class="comments-link">' . '<a href="' . $post->link . '#respond">Leave a comment<span class="screen-reader-text"> on' . $post->title->rendered . '</span>' . '</a>' . '</span>' .'</footer>' .'</article>'; }
     }
?>

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>