<?php
/**
 * Template Name: JSON Posts - WP REST API
 */
 ?>
<?php get_header(); ?>
<?php

?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
            <!--Replace main posts feed on start page by fetching posts from the WP REST API (v2)-->
              <script>
              //function for convert iso 8601 date in day-month-year
              function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                return [day, month, year].join('-');
}
              //posts WP Rest API
                  jQuery(document).ready(function($) {
                    $.get( "http://iqq.pro100photo.in.ua/wp-json/wp/v2/posts", function( data ) {
                        $.each( data, function( i, val ) {
                            
                            $( "#main" ).append( 
                                    '<article id="post-' + val.id + '" class="post-' + val.id + 
                                    ' post type-post status-publish format-standard hentry">' +
                                        '<header class="entry-header">'+ 
                                            '<h2 class="entry-title">' + 
                                                '<a href="' + val.link + '">' + val.title.rendered +'<sup class="apipost"> POST from API</sup>' + '</a>' + 
                                            '</h2>'+ 
                                        '</header>' +
                                        
                                        '<div class="entry-content">' + val.content.rendered + '</div>' +
                                        
                                        '<footer class="entry-footer">'+ 
                                            '<span class="posted-on">' +  
                                                '<a href="' + val.link + '" rel="bookmark">'+ 
                                                    '<time class="entry-date published updated" datetime="' + formatDate(val.date) + '">' + formatDate(val.date) + 
                                                    '</time>' + 
                                                '</a>'+ 
                                            '</span>' +
                                            
                                            '<span class="comments-link">' + 
                                                '<a href="' + val.link + 
                                                '#respond">Leave a comment<span class="screen-reader-text"> on' + val.title.rendered + 
                                                '</span>' + 
                                                '</a>' + 
                                            '</span>' +
                                        '</footer>' + 
                                    '</article>'
                                    

                            );
                        });
                    });
                  });
              </script>
              
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>