This is files from folder **twentysixteen-child**.


**Test tasks:**

1) Create a child theme of twentysixteen.

2) Replace main posts feed on start page by fetching posts from the WP REST API (v2).

3) Add control in WordPress admin for enable/disable printing of your new post feed.

4) Add indicator in each post on start page feed that tells the visitor if the post that is being displayed is fetched from the API.

**Completed tasks:**

1) Create a child theme of twentysixteen - **Done**. All files you can see in this repository.

2) Replace main posts feed on start page by fetching posts from the WP REST API (v2). - **Done**. I created 2 templates: **iqq-jsonapi.php** and **iqq-phpapi.php** .

4) Add indicator in each post on start page feed that tells the visitor if the post that is being displayed is fetched from the API - **Done**

======================================================

All work done can be seen on the website - http://iqq.pro100photo.in.ua/